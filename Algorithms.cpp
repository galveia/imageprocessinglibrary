#include "stdafx.h"
#include <math.h>
#include <iostream>
#include "Algorithms.h"
//using namespace std;

void __stdcall sobel(int width, int height, int size, int padding, unsigned char* data)
{
	unsigned char* new_data = new unsigned char[size];
	
	for (int h = 1; h < height - 1; h = h + 1)
	{
		for (int w = 1; w < width - 1; w = w + 1)
		{
			int Gx = 0;
			int Gy = 0;
			
			int intensity = getIntensityPixel(data, w - 1, h - 1, width, padding);
			Gx += -intensity;
			Gy += -intensity;
			intensity = getIntensityPixel(data, w - 1, h, width, padding);
			Gx += -2 * intensity;
			intensity = getIntensityPixel(data, w - 1, h + 1, width, padding);
			Gx += -intensity;
			Gy += intensity;

			intensity = getIntensityPixel(data, w, h - 1, width, padding);
			Gy += -2 * intensity;
			intensity = getIntensityPixel(data, w, h + 1, width, padding);
			Gy += 2 * intensity;

			intensity = getIntensityPixel(data, w + 1, h - 1, width, padding);
			Gx += intensity;
			Gy += -intensity;
			intensity = getIntensityPixel(data, w + 1, h, width, padding);
			Gx += 2 * intensity;
			intensity = getIntensityPixel(data, w + 1, h + 1, width, padding);
			Gx += intensity;
			Gy += intensity;

			// calculate the length of the gradient (Pythagorean theorem)
			float length = sqrt((Gx * Gx) + (Gy * Gy));

			// normalise the length of gradient to the range 0 to 255
			length = length / 4328 * 255;
			int value = (int)length;

			// draw the length in the edge image
			int index = (h * width * 3) + (h * padding) + (w * 3);
			*(new_data + index) = value;
			*(new_data + index + 1) = value;
			*(new_data + index + 2) = value;
		}
	}

	//data = new_data;
	for (int i = 0; i < size; i = i + 1)
	{
		*(data + i) = *(new_data + i);
	}
}

//unsigned char* getPixel(unsigned char* data, int x, int y, int width, int padding)
//{
//	int index = (y * width * 3) + (y * padding) + (x * 3);
//	return new unsigned char[3] { *(data + index), *(data + index + 1), *(data + index + 2) };
//}

int getIntensityPixel(unsigned char* data, int x, int y, int width, int padding)
{
	int index = (y * width * 3) + (y * padding) + (x * 3);

	int r = *(data + (index + 2));
	int g = *(data + (index + 1));
	int b = *(data + index);

	return r + g + b;
}

int __stdcall test(int* data)
{
	int result = 1;
	for (int a = 0; a < 10; a = a + 1)
	{
		int val = *(data + a);
		if (val == 8)
			*(data + a) = 3;

		result = val;
	}


	int arr[] = { 1,2,3,4,5 };


	return result;
}