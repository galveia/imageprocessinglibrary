#pragma once

extern "C"
{
	__declspec(dllexport) void __stdcall sobel(int width, int height, int size, int padding, unsigned char* data);
	//__declspec(dllexport) int __stdcall sobel(int width, int height, int size, int padding, int* data);
	//void __stdcall sobel(int width, int height, int size, int padding, unsigned int * data);
	__declspec(dllexport) int __stdcall test(int* data);

	//unsigned char* getPixel(unsigned char* data, int x, int y, int width, int padding);
	int getIntensityPixel(unsigned char* data, int x, int y, int width, int padding);
}
